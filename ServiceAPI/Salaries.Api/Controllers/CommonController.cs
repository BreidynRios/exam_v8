﻿using Microsoft.AspNetCore.Mvc;
using Salaries.Core.Interfaces.UseCases;
using Salaries.Core.Messages.SalaryDTO.DTOs;
using Salaries.Utilities.EntityService;
using System.Threading.Tasks;

namespace Salaries.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly ICommonUseCase _commonUseCase;
        public CommonController(ICommonUseCase commonUseCase)
        {
            _commonUseCase = commonUseCase;
        }

        [HttpGet("office")]
        public async Task<ServiceResponse<BaseEntityDTO>> GetOffices()
        {
            return await _commonUseCase.GetAllOffices();
        }

        [HttpGet("division")]
        public async Task<ServiceResponse<BaseEntityDTO>> GetDivisions()
        {
            return await _commonUseCase.GetAllDivisions();
        }

        [HttpGet("position")]
        public async Task<ServiceResponse<BaseEntityDTO>> GetPositions()
        {
            return await _commonUseCase.GetAllPositions();
        }
    }
}
