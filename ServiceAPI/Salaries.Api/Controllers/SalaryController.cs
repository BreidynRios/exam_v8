﻿using Microsoft.AspNetCore.Mvc;
using Salaries.Core.Interfaces.UseCases;
using Salaries.Core.Messages.SalaryDTO.DTOs;
using Salaries.Utilities.EntityService;
using System.Threading.Tasks;

namespace Salaries.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController : ControllerBase
    {
        private readonly ISalaryUseCase _salaryUseCase;

        public SalaryController(ISalaryUseCase salaryUseCase)
        {
            _salaryUseCase = salaryUseCase;
        }

        [HttpGet]
        public async Task<ServiceResponse<EmployeeSalaryDTO>> GetSalaries()
        {
            return await _salaryUseCase.GetAll();
        }

        [HttpGet("filters")]
        public async Task<ServiceResponse<EmployeeSalaryDTO>> GetSalaries([FromQuery] FilterDTO filter)
        {
            return await _salaryUseCase.GetSalariesByFilters(filter);
        }

        [HttpPost("batch")]
        public async Task<ServiceResponse<SalaryDTO>> AddBatchSalary(RequestSalaryListDTO salaryListDto)
        {
            return await _salaryUseCase.AddBatch(salaryListDto);
        }

        [HttpGet("employeecode/{employeeCode}")]
        public async Task<ServiceResponse<EmployeeSalaryDTO>> GetByEmployeeCode(string employeeCode)
        {
            return await _salaryUseCase.GetByEmployeeCode(employeeCode);
        }
    }
}
