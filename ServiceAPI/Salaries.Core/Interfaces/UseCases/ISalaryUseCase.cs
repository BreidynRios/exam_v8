﻿using Salaries.Core.Messages.SalaryDTO.DTOs;
using Salaries.Utilities.EntityService;
using System.Threading.Tasks;

namespace Salaries.Core.Interfaces.UseCases
{
    public interface ISalaryUseCase
    {
        Task<ServiceResponse<EmployeeSalaryDTO>> GetAll();
        Task<ServiceResponse<EmployeeSalaryDTO>> GetSalariesByFilters(FilterDTO filter);
        Task<ServiceResponse<SalaryDTO>> AddBatch(RequestSalaryListDTO salaryListDto);
        Task<ServiceResponse<EmployeeSalaryDTO>> GetByEmployeeCode(string employeeCode);
    }
}
