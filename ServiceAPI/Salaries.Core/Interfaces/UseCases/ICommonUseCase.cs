﻿using Salaries.Core.Messages.SalaryDTO.DTOs;
using Salaries.Utilities.EntityService;
using System.Threading.Tasks;

namespace Salaries.Core.Interfaces.UseCases
{
    public interface ICommonUseCase
    {
        Task<ServiceResponse<BaseEntityDTO>> GetAllOffices();
        Task<ServiceResponse<BaseEntityDTO>> GetAllDivisions();
        Task<ServiceResponse<BaseEntityDTO>> GetAllPositions();
    }
}
