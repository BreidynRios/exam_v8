﻿using AutoMapper;
using Salaries.Core.Interfaces.UseCases;
using Salaries.Core.Messages.SalaryDTO.DTOs;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using Salaries.Utilities.EntityService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Salaries.Core.UseCases
{
    public class SalaryUseCase : ISalaryUseCase
    {
        private readonly ISalaryRepository _salaryRepository;
        private readonly IMapper _mapper;

        public SalaryUseCase(ISalaryRepository salaryRepository, IMapper mapper)
        {
            _salaryRepository = salaryRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<SalaryDTO>> AddBatch(RequestSalaryListDTO salaryListDto)
        {
            ServiceResponse<SalaryDTO> serviceResponse = new ServiceResponse<SalaryDTO>();
            try
            {
                IList<string> lstError = new List<string>();

                var duplicates = salaryListDto.salaryList.GroupBy(x => new { x.EmployeeCode, x.Month, x.Year }).Where(g => g.Count() > 1)
                                                .Select(g => new
                                                {
                                                    g.Key.EmployeeCode,
                                                    g.Key.Month,
                                                    g.Key.Year,
                                                    Count = g.Count()
                                                }).ToList();

                if (duplicates.Count > 0)
                {
                    foreach (var item in duplicates)
                    {
                        lstError.Add($"Hay {item.Count} filas del empleado con código {item.EmployeeCode} " +
                            $"en el mes de {(Utilities.Enumerations.Month)item.Month} del año {item.Year}");
                    }

                    serviceResponse.Errors = lstError;
                    return serviceResponse;
                }
                else
                {
                    IList<Salary> lstSalary = new List<Salary>();

                    foreach (var item in salaryListDto.salaryList)
                    {
                        var salary = _mapper.Map<Salary>(item);
                        lstSalary.Add(salary);

                        var exists = await _salaryRepository.FindByMonthAndYear(salary);
                        if (exists)
                        {
                            lstError.Add($"El empleado {item.EmployeeCode} - {item.EmployeeName} {item.EmployeeSurname} " +
                                $"ya tiene registrado un salario en el mes de {(Utilities.Enumerations.Month)item.Month} del año {item.Year}");
                        }
                    }

                    if (lstError.Count > 0)
                    {
                        serviceResponse.Errors = lstError;
                        return serviceResponse;
                    }
                    else
                    {
                        await _salaryRepository.AddBatch(lstSalary);                        
                        serviceResponse.DataList = _mapper.Map<IList<SalaryDTO>>(lstSalary);
                        serviceResponse.Message = "Los salarios se guardaron de manera exitosa";
                        serviceResponse.Success = true;
                    }
                }
            }
            catch (Exception)
            {
                serviceResponse.Message = Utilities.Enumerations.Messages.CatchError;
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeSalaryDTO>> GetAll()
        {
            ServiceResponse<EmployeeSalaryDTO> serviceResponse = new ServiceResponse<EmployeeSalaryDTO>();

            try
            {
                var salaries = await _salaryRepository.GetAll();
                var employeeSalaries = GetEmployeeSalaries(salaries.ToList());

                serviceResponse.DataList = _mapper.Map<IList<EmployeeSalaryDTO>>(employeeSalaries);
                serviceResponse.Success = true;
            }
            catch (Exception)
            {
                serviceResponse.Message = Utilities.Enumerations.Messages.CatchError;
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeSalaryDTO>> GetSalariesByFilters(FilterDTO filter)
        {
            ServiceResponse<EmployeeSalaryDTO> serviceResponse = new ServiceResponse<EmployeeSalaryDTO>();

            try
            {
                var salaries = await _salaryRepository.GetSalariesByFilters();

                if (filter.Office > 0 && filter.Grade > 0)
                {
                    salaries = salaries.Where(x => x.Office.Id == filter.Office && x.Grade == filter.Grade);
                }
                else if (filter.Office == 0 && filter.Position == 0 && filter.Grade > 0)
                {
                    salaries = salaries.Where(x => x.Grade == filter.Grade);
                }
                else if (filter.Position > 0 && filter.Grade > 0)
                {
                    salaries = salaries.Where(x => x.Position.Id == filter.Position && x.Grade == filter.Grade);
                }
                else
                {
                    serviceResponse.Message = "Seleccione los filtros a aplicar";
                    return serviceResponse;
                }

                var employeeSalaries = GetEmployeeSalaries(salaries.ToList());

                serviceResponse.DataList = _mapper.Map<IList<EmployeeSalaryDTO>>(employeeSalaries);
                serviceResponse.Success = true;
            }
            catch (Exception)
            {
                serviceResponse.Message = Utilities.Enumerations.Messages.CatchError;
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeSalaryDTO>> GetByEmployeeCode(string employeeCode)
        {
            ServiceResponse<EmployeeSalaryDTO> serviceResponse = new ServiceResponse<EmployeeSalaryDTO>();

            try
            {
                var salaries = await _salaryRepository.GetByEmployeeCode(employeeCode);

                if (salaries.Count() > 0)
                {
                    var salariesLastThreeMonths = salaries.OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).Take(3);

                    var salariesDto = _mapper.Map<IList<SalaryDTO>>(salariesLastThreeMonths);
                    var employeeSalaries = GetTotalSalary(salariesDto);

                    decimal bonus = employeeSalaries.Sum(x => x.TotalSalary) / 3;

                    serviceResponse.Message = $"El bono del empleado es: {bonus.ToString("F", CultureInfo.InvariantCulture)}";
                    serviceResponse.DataList = _mapper.Map<IList<EmployeeSalaryDTO>>(employeeSalaries);
                    serviceResponse.Success = true;
                }
                else
                {
                    serviceResponse.Message = "No existen salarios con el código de empleado ingresado";
                }
            }
            catch (Exception)
            {
                serviceResponse.Message = Utilities.Enumerations.Messages.CatchError;
            }

            return serviceResponse;
        }

        private IList<SalaryDTO> GetEmployeeSalaries(IList<Salary> salaries)
        {
            var salariesDto = _mapper.Map<IList<SalaryDTO>>(salaries);
            salariesDto = salariesDto.OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).ToList();
            salariesDto = GetTotalSalary(salariesDto);

            var groupSalaries = salariesDto.GroupBy(g => g.EmployeeCode).Select(x => x.First()).ToList();

            _ = groupSalaries.Select(x =>
            {
                x.TotalSalary = salariesDto.Where(s => s.EmployeeCode == x.EmployeeCode).Sum(s => s.TotalSalary);
                return x;
            }).ToList();

            return groupSalaries;
        }

        private IList<SalaryDTO> GetTotalSalary(IList<SalaryDTO> salaries)
        {
            foreach (var item in salaries)
            {
                decimal otherIncome = (item.BaseSalary + item.Commission) * 0.08M + item.Commission;
                decimal totalSalary = item.BaseSalary + item.ProductionBonus + (item.CompensationBonus * 0.75M) + otherIncome - item.Contributions;
                item.TotalSalary = totalSalary;
            }

            return salaries;
        }
    }
}
