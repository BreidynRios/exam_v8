﻿using AutoMapper;
using Salaries.Core.Interfaces.UseCases;
using Salaries.Core.Messages.SalaryDTO.DTOs;
using Salaries.Repository.Interfaces;
using Salaries.Utilities.EntityService;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Core.UseCases
{
    public class CommonUseCase : ICommonUseCase
    {
        private readonly IOfficeRepository _officeRepository;
        private readonly IDivisionRepository _divisionRepository;
        private readonly IPositionRepository _positionRepository;
        private readonly IMapper _mapper;

        public CommonUseCase(IOfficeRepository officeRepository, IDivisionRepository divisionRepository, 
            IPositionRepository positionRepository, IMapper mapper)
        {
            _officeRepository = officeRepository;
            _divisionRepository = divisionRepository;
            _positionRepository = positionRepository;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<BaseEntityDTO>> GetAllDivisions()
        {
            ServiceResponse<BaseEntityDTO> serviceResponse = new ServiceResponse<BaseEntityDTO>();

            try
            {
                var divisions = await _divisionRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDTO>>(divisions);
                serviceResponse.Success = true;
            }
            catch (Exception)
            {
                serviceResponse.Message = Utilities.Enumerations.Messages.CatchError;
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<BaseEntityDTO>> GetAllOffices()
        {
            ServiceResponse<BaseEntityDTO> serviceResponse = new ServiceResponse<BaseEntityDTO>();

            try
            {
                var divisions = await _officeRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDTO>>(divisions);
                serviceResponse.Success = true;
            }
            catch (Exception)
            {
                serviceResponse.Message = Utilities.Enumerations.Messages.CatchError;
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<BaseEntityDTO>> GetAllPositions()
        {
            ServiceResponse<BaseEntityDTO> serviceResponse = new ServiceResponse<BaseEntityDTO>();

            try
            {
                var divisions = await _positionRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDTO>>(divisions);
                serviceResponse.Success = true;
            }
            catch (Exception)
            {
                serviceResponse.Message = Utilities.Enumerations.Messages.CatchError;
            }

            return serviceResponse;
        }
    }
}
