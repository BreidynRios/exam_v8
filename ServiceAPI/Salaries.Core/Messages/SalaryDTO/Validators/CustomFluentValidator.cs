﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Salaries.Core.Messages.SalaryDTO.Validators
{
    public abstract class CustomFluentValidator<T> : AbstractValidator<T>
    {
        public override ValidationResult Validate(ValidationContext<T> context)
        {
            var result = base.Validate(context);
            FixIndexedPropertyErrorMessage(result);

            return result;
        }
        public override async Task<ValidationResult> ValidateAsync(ValidationContext<T> context, CancellationToken cancellation = default(CancellationToken))
        {
            var result = await base.ValidateAsync(context, cancellation);
            FixIndexedPropertyErrorMessage(result);

            return result;
        }

        protected void FixIndexedPropertyErrorMessage(ValidationResult result)
        {
            if (result.Errors?.Any() ?? false)
            {
                foreach (var error in result.Errors)
                {
                    var index = Convert.ToInt32(error.FormattedMessagePlaceholderValues["CollectionIndex"]) + 1;
                    error.ErrorMessage = $"Fila {index}: {error.ErrorMessage}";
                }
            }
        }
    }
}
