﻿using FluentValidation;
using Salaries.Core.Messages.SalaryDTO.DTOs;
using System;

namespace Salaries.Core.Messages.SalaryDTO.Validators
{
    public class SalaryValidator : AbstractValidator<DTOs.SalaryDTO>
    {
        public SalaryValidator()
        {
            RuleFor(salary => salary.Year)
                .GreaterThan(0)
                .WithMessage("Ingrese un año válido");

            RuleFor(salary => salary.Month)
                .GreaterThan(0)
                .WithMessage("Ingrese un mes válido");

            RuleFor(salary => salary.OfficeId)
                .GreaterThan(0)
                .WithMessage("Ingrese una oficina válida");

            RuleFor(salary => salary.EmployeeCode)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Ingrese el código de empleado")
                .Length(1, 10)
                .WithMessage("La longitud del código de empleado no debe ser mayor a 10 caracteres");

            RuleFor(salary => salary.EmployeeName)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Ingrese el nombre de empleado")
                .Length(1, 150)
                .WithMessage("La longitud del nombre de empleado no debe ser mayor a 150 caracteres");

            RuleFor(salary => salary.EmployeeSurname)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Ingrese el apellido de empleado")
                .Length(1, 150)
                .WithMessage("La longitud del apellido de empleado no debe ser mayor a 150 caracteres");

            RuleFor(salary => salary.DivisionId)
                .GreaterThan(0)
                .WithMessage("Ingrese una división válida");

            RuleFor(salary => salary.PositionId)
                .GreaterThan(0)
                .WithMessage("Ingrese un puesto válido");

            RuleFor(salary => salary.Grade)
                .GreaterThan(0)
                .WithMessage("Ingrese un grado válido");

            RuleFor(salary => salary.BeginDate)
                .Cascade(CascadeMode.Stop)
                .NotNull()
                .WithMessage("Ingrese una fecha de inicio")
                .Must(x => BeAValidDate(x.Value))
                .WithMessage("Ingrese una fecha de inicio válida");

            RuleFor(salary => salary.Birthday)
                .Cascade(CascadeMode.Stop)
                .NotNull()
                .WithMessage("Ingrese una fecha de cumpleaños")
                .Must(x => BeAValidDate(x.Value))
                .WithMessage("Ingrese una fecha de cumpleaños válida");

            RuleFor(salary => salary.IdentificationNumber)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Ingrese el número de identificación")
                .Length(1, 10)
                .WithMessage("La longitud del número de identificación no debe ser mayor a 10 caracteres");

            RuleFor(salary => salary.BaseSalary)
                .GreaterThan(-1)
                .WithMessage("El salario base no puede ser menor que 0");

            RuleFor(salary => salary.ProductionBonus)
                .GreaterThan(-1)
                .WithMessage("El bono de producción no puede ser menor que 0");

            RuleFor(salary => salary.CompensationBonus)
                .GreaterThan(-1)
                .WithMessage("El bono de compensación no puede ser menor que 0");

            RuleFor(salary => salary.Commission)
                .GreaterThan(-1)
                .WithMessage("La comisión no puede ser menor que 0");

            RuleFor(salary => salary.Contributions)
                .GreaterThan(-1)
                .WithMessage("La Contribución no puede ser menor que 0");
        }

        private bool BeAValidDate(DateTime date)
        {
            return !date.Equals(default);
        }
    }

    public class SalaryValidatorList : CustomFluentValidator<RequestSalaryListDTO>
    {
        public SalaryValidatorList()
        {
            RuleForEach(x => x.salaryList).SetValidator(new SalaryValidator());
        }
    }

}
