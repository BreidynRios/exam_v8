﻿using System.Collections.Generic;

namespace Salaries.Core.Messages.SalaryDTO.DTOs
{
    public class RequestSalaryListDTO
    {
        public IList<SalaryDTO> salaryList { get; set; }
    }
}
