﻿namespace Salaries.Core.Messages.SalaryDTO.DTOs
{
    public class FilterDTO
    {
        public byte Office { get; set; }
        public byte Grade { get; set; }
        public byte Position { get; set; }
    }
}
