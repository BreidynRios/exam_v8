﻿using System;

namespace Salaries.Core.Messages.SalaryDTO.DTOs
{
    public class EmployeeSalaryDTO
    {
        public string EmployeeCode { get; set; }
        public string EmployeeFullName { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal TotalSalary { get; set; }
    }
}
