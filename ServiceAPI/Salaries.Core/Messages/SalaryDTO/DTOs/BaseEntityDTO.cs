﻿namespace Salaries.Core.Messages.SalaryDTO.DTOs
{
    public class BaseEntityDTO
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
