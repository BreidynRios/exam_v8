﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Salaries.Core.Interfaces.UseCases;
using Salaries.Core.UseCases;
using Salaries.DataAccess.Data;
using Salaries.Repository.Interfaces;
using Salaries.Repository.Repositories;

namespace Salaries.Core.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SalaryDBContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("SalaryDB"))
            );

            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            #region Use Cases
            services.AddTransient<ISalaryUseCase, SalaryUseCase>();
            services.AddTransient<ICommonUseCase, CommonUseCase>();
            #endregion

            #region Repositories
            services.AddTransient<ISalaryRepository, SalaryRepository>();
            services.AddTransient<IDivisionRepository, DivisionRepository>();
            services.AddTransient<IOfficeRepository, OfficeRepository>();
            services.AddTransient<IPositionRepository, PositionRepository>();
            #endregion

            return services;
        }
    }
}
