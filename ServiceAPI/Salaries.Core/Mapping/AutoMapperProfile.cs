﻿using AutoMapper;
using Salaries.Core.Messages.SalaryDTO.DTOs;
using Salaries.Entity;

namespace Salaries.Core.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Salary, SalaryDTO>()
                .ForMember(d => d.Position, opt => opt.MapFrom(src => src.Position.Name))
                .ForMember(d => d.Office, opt => opt.MapFrom(src => src.Office.Name))
                .ForMember(d => d.Division, opt => opt.MapFrom(src => src.Division.Name))
                .ReverseMap()
                .ForPath(s => s.Position.Name, opt => opt.Ignore())
                .ForPath(s => s.Office.Name, opt => opt.Ignore())
                .ForPath(s => s.Division.Name, opt => opt.Ignore());

            CreateMap<SalaryDTO, EmployeeSalaryDTO>()
                .ForMember(d => d.EmployeeFullName, opt => opt.MapFrom(src => $"{src.EmployeeName} {src.EmployeeSurname}"));

            CreateMap<Office, BaseEntityDTO>().ReverseMap();
            CreateMap<Division, BaseEntityDTO>().ReverseMap();
            CreateMap<Position, BaseEntityDTO>().ReverseMap();
        }
    }
}
