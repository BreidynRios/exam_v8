﻿using System.Collections.Generic;

namespace Salaries.Utilities.EntityService
{
    public class ServiceResponse<T>
    {
        public bool Success { get; set; }
        public T Data { get; set; }
        public IList<T> DataList { get; set; }
        public IEnumerable<string> Errors { get; set; }
        public string Message { get; set; }
    }
}
