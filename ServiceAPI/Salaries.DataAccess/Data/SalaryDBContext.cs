﻿using Microsoft.EntityFrameworkCore;
using Salaries.Entity;
using System.Reflection;

namespace Salaries.DataAccess.Data
{
    public partial class SalaryDBContext : DbContext
    {
        public SalaryDBContext() { }
        public SalaryDBContext(DbContextOptions<SalaryDBContext> options) : base(options) { }
        public virtual DbSet<Salary> Salaries { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<Division> Divisions { get; set; }
        public virtual DbSet<Position> Positions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
