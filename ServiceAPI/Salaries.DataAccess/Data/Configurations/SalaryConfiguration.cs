﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Salaries.Entity;

namespace Salaries.DataAccess.Data.Configurations
{
    public class SalaryConfiguration : IEntityTypeConfiguration<Salary>
    {
        public void Configure(EntityTypeBuilder<Salary> builder)
        {
            builder.ToTable("Salary");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Year)
                .HasColumnName("Year")
                .IsRequired();

            builder.Property(e => e.Month)
                .HasColumnName("Month")
                .IsRequired();

            builder.Property(e => e.OfficeId)
                .HasColumnName("Office")
                .IsRequired();

            builder.Property(e => e.EmployeeCode)
                .HasColumnName("EmployeeCode")
                .HasMaxLength(10)
                .IsRequired();

            builder.Property(e => e.EmployeeName)
                .HasColumnName("EmployeeName")
                .HasMaxLength(150)
                .IsRequired();

            builder.Property(e => e.EmployeeSurname)
                .HasColumnName("EmployeeSurname")
                .HasMaxLength(150)
                .IsRequired();

            builder.Property(e => e.DivisionId)
                .HasColumnName("Division")
                .IsRequired();

            builder.Property(e => e.PositionId)
                .HasColumnName("Position")
                .IsRequired();

            builder.Property(e => e.Grade)
                .HasColumnName("Grade")
                .IsRequired();

            builder.Property(e => e.BeginDate)
                .HasColumnName("BeginDate")
                .HasColumnType("date")
                .IsRequired();

            builder.Property(e => e.Birthday)
                .HasColumnName("Birthday")
                .HasColumnType("date")
                .IsRequired();

            builder.Property(e => e.IdentificationNumber)
                .HasColumnName("IdentificationNumber")
                .HasMaxLength(10)
                .IsRequired();

            builder.Property(e => e.BaseSalary)
                .HasColumnName("BaseSalary")
                .HasColumnType("decimal(18,2)")
                .IsRequired();

            builder.Property(e => e.ProductionBonus)
                .HasColumnName("ProductionBonus")
                .HasColumnType("decimal(18,2)")
                .IsRequired();

            builder.Property(e => e.CompensationBonus)
                .HasColumnName("CompensationBonus")
                .HasColumnType("decimal(18,2)")
                .IsRequired();

            builder.Property(e => e.Commission)
                .HasColumnName("Commission")
                .HasColumnType("decimal(18,2)")
                .IsRequired();

            builder.Property(e => e.Contributions)
                .HasColumnName("Contributions")
                .HasColumnType("decimal(18,2)")
                .IsRequired();
        }
    }
}
