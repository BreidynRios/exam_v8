﻿using Salaries.Entity;

namespace Salaries.Repository.Interfaces
{
    public interface IDivisionRepository : IBaseRepository<Division>
    {

    }
}
