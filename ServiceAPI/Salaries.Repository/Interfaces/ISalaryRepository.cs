﻿using Salaries.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Salaries.Repository.Interfaces
{
    public interface ISalaryRepository : IBaseRepository<Salary>
    {
        Task<IQueryable<Salary>> GetSalariesByFilters();
        Task AddBatch(IEnumerable<Salary> salaries);
        Task<bool> FindByMonthAndYear(Salary salaries);
        Task<IEnumerable<Salary>> GetByEmployeeCode(string employeeCode);
    }
}
