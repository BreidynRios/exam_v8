﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Repository.Interfaces
{
    public interface IBaseRepository<T>
    {
        Task<IEnumerable<T>> GetAll();
    }
}
