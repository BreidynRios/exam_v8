﻿using Salaries.Entity;

namespace Salaries.Repository.Interfaces
{
    public interface IPositionRepository : IBaseRepository<Position>
    {
    }
}
