﻿using Salaries.Entity;

namespace Salaries.Repository.Interfaces
{
    public interface IOfficeRepository : IBaseRepository<Office>
    {

    }
}
