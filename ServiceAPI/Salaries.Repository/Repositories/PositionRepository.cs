﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataAccess.Data;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Repository.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        private readonly SalaryDBContext _context;
        public PositionRepository(SalaryDBContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Position>> GetAll()
        {
            var positions = await _context.Positions.ToListAsync();
            return positions;
        }
    }
}
