﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataAccess.Data;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Repository.Repositories
{
    public class DivisionRepository : IDivisionRepository
    {
        private readonly SalaryDBContext _context;
        public DivisionRepository(SalaryDBContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Division>> GetAll()
        {
            var divisions = await _context.Divisions.ToListAsync();
            return divisions;
        }
    }
}
