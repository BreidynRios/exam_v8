﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataAccess.Data;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Repository.Repositories
{
    public class OfficeRepository : IOfficeRepository
    {
        private readonly SalaryDBContext _context;
        public OfficeRepository(SalaryDBContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Office>> GetAll()
        {
            var offices = await _context.Offices.ToListAsync();
            return offices;
        }
    }
}
