﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataAccess.Data;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Salaries.Repository.Repositories
{
    public class SalaryRepository : ISalaryRepository
    {
        private readonly SalaryDBContext _context;
        public SalaryRepository(SalaryDBContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Salary>> GetAll()
        {
            var salaries = await _context.Salaries
                                    .Include(x => x.Division)
                                    .Include(x => x.Office)
                                    .Include(x => x.Position)
                                    .ToListAsync();
            return salaries;
        }

        public async Task<IQueryable<Salary>> GetSalariesByFilters()
        {
            var salaries = _context.Salaries
                                    .Include(x => x.Division)
                                    .Include(x => x.Office)
                                    .Include(x => x.Position).AsQueryable();

            return salaries;
        }

        public async Task AddBatch(IEnumerable<Salary> salaries)
        {
            await _context.Salaries.AddRangeAsync(salaries);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> FindByMonthAndYear(Salary salaries)
        {
            var exists = await _context.Salaries.AnyAsync(x => x.EmployeeCode == salaries.EmployeeCode 
                                                && x.Month == salaries.Month && x.Year == salaries.Year);
            return exists;
        }

        public async Task<IEnumerable<Salary>> GetByEmployeeCode(string employeeCode)
        {
            var salaries = await _context.Salaries.Where(x => x.EmployeeCode == employeeCode)
                                    .Include(x => x.Division)
                                    .Include(x => x.Office)
                                    .Include(x => x.Position)
                                    .ToListAsync();
            return salaries;
        }

    }
}
