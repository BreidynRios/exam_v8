﻿namespace Salaries.Entity
{
    public class BaseEntity
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
