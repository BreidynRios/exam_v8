# Exam in .NET Core and Angular

This webproject runs over ASPNET Core with EntityFrameworkCore

## Requeriments

* Visual Studio 2019
* IIS Express
* SQL Server 
* VS Code
* Angular CLI: 11.0.2
* Node: 14.15.1

## Connection strings

Feel free to use another sql connection string

```
Data Source=(LocalDb)\\MSSQLLocalDB;Initial Catalog=SalaryDB;Integrated Security=True;Pooling=False
```

## Instructions

1. Run the script from "ScriptBD.sql" in SQL Server.
2. Make sure that script has been executed correctly
3. Open solution (.sln) with Visual Studio
4. Now run the proyect using Visual Studio (make sure that Salaries.Api is init project)
5. Open solution "SalaryApp" in VS Code.
6. Open the terminal in VS Code and execute the command "npm install", after "ng serve -o".