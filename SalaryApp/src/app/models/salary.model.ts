
export class SalaryModel {
    year: number;
    month: number;
    employeeCode: string;
    employeeName: string;
    employeeSurname: string;
    officeId: number;
    divisionId: number;
    positionId: number;
    grade: string;
    beginDate: string;
    birthday: string;
    identificationNumber: string;
    baseSalary: number;
    productionBonus: number;
    compensationBonus: number;
    commission: number;
    contributions: number;
}

export class SalaryListModel {
    salaryList : SalaryModel[];
}