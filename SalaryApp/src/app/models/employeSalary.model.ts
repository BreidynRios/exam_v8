
export class EmployeeSalaryModel{
    employeeCode: string;
    employeeFullName: string;
    division: string;
    position: string;
    beginDate: Date;
    birthday: Date;
    identificationNumber: string;
    totalSalary: number;
}