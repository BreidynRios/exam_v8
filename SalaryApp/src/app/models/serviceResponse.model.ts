
export class ServiceResponseModel {
    success: boolean;
    message: string;
    dataList: any;
    errors: any;
}