import { Component, OnInit } from '@angular/core';
import { EmployeeSalaryModel } from '../../models/employeSalary.model';
import { SalariesService } from '../../services/salaries.service';
import { ServiceResponseModel } from '../../models/serviceResponse.model';
import { BaseEntityModel } from '../../models/baseEntity.model';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styles: [
  ]
})
export class EmployeeComponent implements OnInit {

  salaries : EmployeeSalaryModel[] = [];
  isError : boolean;
  message : string;
  employeeCode : string;
  infoBonus: string = "";
  gradeOfficeId: number;
  gradePositionId: number;
  officeId: number = 0;
  positionId: number = 0;
  offices: BaseEntityModel[] = [];
  positions: BaseEntityModel[] = [];


  constructor( private salariesService: SalariesService) { }

  ngOnInit(): void {    
    this.getSalary();

    this.salariesService.getOffices().subscribe( (resp : ServiceResponseModel) => {      
      if ( resp.success ) {
        this.offices = resp.dataList;
      }
    });

    this.salariesService.getPositions().subscribe( (resp : ServiceResponseModel) => {      
      if ( resp.success ) {
        this.positions = resp.dataList;
      }
    });
  }

  getSalary() {
    this.infoBonus = "";

    this.salariesService.getSalaries().subscribe( (resp : ServiceResponseModel) => {
      
      if ( resp.success ) {
        this.salaries = resp.dataList;
      } else {
        this.isError = true;
        this.message = resp.message;
      }

    });
  }

  getSalariesByEmployeeCode() {
    this.isError = false;
    this.infoBonus = "";

    if ( (this.employeeCode || "") === "" ) {
        this.isError = true;
        this.message = "Ingrese el código del empleado";
    } else {
      this.salariesService.getSalariesByEmployeeCode(this.employeeCode).subscribe( (resp : ServiceResponseModel) => {
      
        if ( resp.success ) {
          this.salaries = resp.dataList;
          this.infoBonus = resp.message;
        } else {
          this.isError = true;
          this.message = resp.message;
          this.salaries = [];
        }
  
      });
    }
    
  }

  getSalariesByFilters( type : string ) {

    var officeId = 0;
    var gradeId = 0;
    var positionId = 0;

    if ( type === 'o') {
      officeId = this.officeId;
      gradeId = this.gradeOfficeId;
    } else {
      positionId = this.positionId;
      gradeId = this.gradePositionId;
    }

    this.salariesService.getSalariesByFilters(officeId, gradeId, positionId).subscribe( (resp : ServiceResponseModel) => {
      if ( resp.success ) {
        this.salaries = resp.dataList;
        this.infoBonus = resp.message;
      } else {
        this.isError = true;
        this.message = resp.message;
        this.salaries = [];
      }

    });

  }

}
