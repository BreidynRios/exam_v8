import { Component, OnInit } from '@angular/core';
import { SalariesService } from '../../services/salaries.service';
import { ServiceResponseModel } from '../../models/serviceResponse.model';
import { BaseEntityModel } from '../../models/baseEntity.model';
import { SalaryModel, SalaryListModel } from 'src/app/models/salary.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent implements OnInit {

  salaries : SalaryModel[] = [];
  offices: BaseEntityModel[] = [];
  divisiones: BaseEntityModel[] = [];
  positions: BaseEntityModel[] = [];
  years: number[] = [];
  months: BaseEntityModel[] = [];
  message: string = "";
  isSuccess: boolean;
  errorsMessages: string[] = [];
  currentYear = (new Date()).getFullYear();

  constructor( private salariesService: SalariesService ) { }

  ngOnInit(): void {

    this.getMonths();
    this.getYears();

    this.salariesService.getOffices().subscribe( (resp : ServiceResponseModel) => {      
      if ( resp.success ) {
        this.offices = resp.dataList;
      }
    });

    this.salariesService.getDivisions().subscribe( (resp : ServiceResponseModel) => {      
      if ( resp.success ) {
        this.divisiones = resp.dataList;
      }
    });

    this.salariesService.getPositions().subscribe( (resp : ServiceResponseModel) => {      
      if ( resp.success ) {
        this.positions = resp.dataList;
      }
    });
  }

  addSalary() {
    this.salaries.push({
      year : this.currentYear,
      month : 1,
      employeeCode : "",
      employeeName : "",
      employeeSurname : "",
      officeId : 1,
      divisionId : 1,
      positionId : 1,
      grade: "",
      beginDate: "",
      birthday: "",
      identificationNumber: "",
      baseSalary: 0,
      productionBonus: 0,
      compensationBonus: 0,
      commission: 0,
      contributions: 0,
    });
  }

  saveSalaries() {
    this.message = "";
    this.isSuccess = false;
    this.errorsMessages = [];

    if (this.salaries.length === 0) {
      this.message = "Ingrese los salarios a registrar";
    } else {

      const salaryListModel = new SalaryListModel();
      salaryListModel.salaryList = this.salaries;

      try {
        this.salariesService.saveSalaries(salaryListModel).subscribe( 
          (resp : ServiceResponseModel) => {
            if ( resp.success ) {
              this.message = resp.message;
            } else {
              this.errorsMessages = resp.errors;
            }

            this.isSuccess = resp.success;
          }, (errorService) => {
            this.errorsMessages = errorService.error.message;
          }
        );
      } catch { }
    }
  }

  getMonths() {
    this.months.push({ id : 1, name: "Enero"  });
    this.months.push({ id : 2, name: "Febrero"  });
    this.months.push({ id : 3, name: "Marzo"  });
    this.months.push({ id : 4, name: "Abril"  });
    this.months.push({ id : 5, name: "Mayo"  });
    this.months.push({ id : 6, name: "Junio"  });
    this.months.push({ id : 7, name: "Julio"  });
    this.months.push({ id : 8, name: "Agosto"  });
    this.months.push({ id : 9, name: "Setiembre"  });
    this.months.push({ id : 10, name: "Octubre"  });
    this.months.push({ id : 11, name: "Noviembre"  });
    this.months.push({ id : 11, name: "Diciembre"  });
  }

  getYears() {
    const minYear = environment.minYear;

    for (var i = minYear; i <= this.currentYear; i++) {
      this.years.push(i);
    }

  }

  removeRow( position: number ) {
    this.salaries.splice(position, 1);
  }

}
