import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './pages/register/register.component';
import { EmployeeComponent } from './pages/employee/employee.component';

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'register' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
