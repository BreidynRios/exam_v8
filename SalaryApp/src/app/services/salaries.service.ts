import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { SalaryListModel } from '../models/salary.model';

@Injectable({
  providedIn: 'root'
})
export class SalariesService {

  constructor( private http: HttpClient ) { }

  getSalaries() {
    return this.http.get(`${ environment.API_URL }/api/salary`);
  }

  getSalariesByEmployeeCode( employeeCode : string ) {
    return this.http.get(`${ environment.API_URL }/api/salary/employeecode/${ employeeCode }`);
  }

  getSalariesByFilters( office: number, grade: number, position: number ) {
    return this.http.get(`${ environment.API_URL }/api/salary/filters?office=${ office }&grade=${ grade }&position=${ position }`);
  }

  getOffices() {
    return this.http.get(`${ environment.API_URL }/api/common/office`);
  }

  getPositions() {
    return this.http.get(`${ environment.API_URL }/api/common/position`);
  }

  getDivisions() {
    return this.http.get(`${ environment.API_URL }/api/common/division`);
  }

  saveSalaries( salaries : SalaryListModel ) {
    return this.http.post(`${ environment.API_URL }/api/salary/batch`, salaries);
  }

}
